package com.user.demo.config;

import com.mongodb.MongoClient;
import com.user.demo.user.internal.UserRepository;
import cz.jirutka.spring.embedmongo.EmbeddedMongoFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.env.Environment;
import org.springframework.data.mapping.model.SimpleTypeHolder;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.core.mapping.MongoSimpleTypes;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Configuration
@EnableMongoRepositories(basePackageClasses = UserRepository.class)
@EnableAutoConfiguration
public class MongoConfig {

    @Autowired
    private Environment env;

    private static final String MONGO_DB_URL = "localhost";
    private static final String MONGO_DB_NAME = "embeded_db";
    @Bean
    public MongoTemplate mongoTemplate() throws IOException {
        EmbeddedMongoFactoryBean mongo = new EmbeddedMongoFactoryBean();
        mongo.setBindIp(MONGO_DB_URL);
        MongoClient mongoClient = mongo.getObject();
        MongoTemplate mongoTemplate = new MongoTemplate(mongoClient, MONGO_DB_NAME);
        return mongoTemplate;
    }

    @Bean
    public MongoMappingContext mongoMappingContext() {
        MongoMappingContext context = new MongoMappingContext();
        context.setSimpleTypeHolder(new SimpleTypeHolder(new HashSet<>(Arrays.asList(
                LocalDate.class,
                LocalDateTime.class
        )), MongoSimpleTypes.HOLDER));
        return context;
    }

    @Bean
    public MongoCustomConversions customConversions() {
        List<Converter<?, ?>> converterList = new ArrayList<Converter<?, ?>>();
        converterList.add(new DateToLocalDateConverter());
        converterList.add(new DateToLocalDateTimeConverter());
        return new MongoCustomConversions(converterList);
    }


    private static final class  DateToLocalDateTimeConverter implements Converter<Date, LocalDateTime> {

        @Override
        public LocalDateTime convert(Date source) {
            return source == null
                    ? null
                    : LocalDateTime.ofInstant(source.toInstant(), ZoneId.systemDefault());
        }
    }

    private static final class  DateToLocalDateConverter implements Converter<Date, LocalDate> {

        @Override
        public LocalDate convert(Date source) {
            return source == null
                    ? null
                    : source.toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        }
    }
}
