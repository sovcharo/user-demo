package com.user.demo.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;


@Api(description = "CRUD resource for user management.")
@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface UserService {

    @GET
    @Path("/")
    @ApiOperation("Retrieves all users.")
    Collection<User> findAllUsers();

    @POST
    @Path("/")
    @ApiOperation("Creates a single user.")
    Long createUser(@Valid User user);

    @GET
    @Path("/{id}")
    @ApiOperation("Retrieves a single user.")
    User findUser(@NotNull @PathParam("id") Long id);

    @PUT
    @Path("/{id}")
    @ApiOperation("Updates a single user.")
    void updateUser(@Valid User user);

    @DELETE
    @Path("/{id}")
    @ApiOperation("Deletes a user.")
    void deleteUser(@NotNull @PathParam("id") Long id);
}
