package com.user.demo.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable {

    @ApiModelProperty(name = "userId")
    private Long id;

    @ApiModelProperty(name = "firstName")
    @NotNull
    private String firstName;

    @ApiModelProperty(name = "lastName")
    @NotNull
    private String lastName;

    @ApiModelProperty(name = "email")
    @Email
    @NotNull
    private  String email;

    @NotNull
    @ApiModelProperty(name = "dateOfBirth")
    private LocalDate dateOfBirth;

    @Override
    public String toString() {
        return ToStringBuilder
                .reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == null) return false;
        if(!(obj instanceof User)) return false;

        return new EqualsBuilder()
                .append(getId(), ((User) obj).getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return getId()!= null ? getId().hashCode() : 0;
    }
}
