package com.user.demo.user.internal;


import com.user.demo.exceptions.SequenceLoadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public class UserSequenceMongoRepository implements UserSequenceRepository {

    @Autowired
    private MongoOperations mongoOperations;

    @Override
    public long getNextId(String key) throws SequenceLoadException{
        createFirstRecordIfEmpty(key);
        Query query = new Query(Criteria.where("_id").is(key));

        //increase sequence id by 1
        Update update = new Update();
        update.inc("seq", 1);

        //return new increased id
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);

        UserSequence seqId =
                mongoOperations.findAndModify(query, update, options, UserSequence.class);

        if (seqId == null) {
            throw new SequenceLoadException(String.format("Unable to get sequence id for key : %s", key));
        }

        return seqId.getSeq();
    }

    private void createFirstRecordIfEmpty(String key) {

        Collection<UserSequence> seqIds = mongoOperations.findAll(UserSequence.class);

        if(seqIds.isEmpty()) {

            mongoOperations.insert(UserSequence
                                        .builder()
                                        .id(key)
                                        .seq(0L)
                                        .build());
        }
    }
}
