package com.user.demo.user.internal;

import com.user.demo.exceptions.SequenceLoadException;

public interface UserSequenceRepository {

    long getNextId(String key) throws SequenceLoadException;
}
