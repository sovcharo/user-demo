package com.user.demo.user.internal;

import com.user.demo.user.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="userSequence")
@Getter
@Setter
@Builder
public class UserSequence {

    @Id
    private String id;

    private long seq;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == null) return false;
        if(!(obj instanceof User)) return false;

        return new EqualsBuilder()
                .append(getId(), ((UserSequence) obj).getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
       return getId() != null ? getId().hashCode() : 0;
    }
}
