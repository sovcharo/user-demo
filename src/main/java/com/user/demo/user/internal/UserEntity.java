package com.user.demo.user.internal;

import com.user.demo.user.User;
import io.swagger.annotations.ApiModel;
import lombok.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Document(collection = "user")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "User basic info.")
class UserEntity {

    @Id
    private Long id;


    private String firstName;


    private String lastName;

    private  String email;


    private LocalDate dateOfBirth;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object obj) {

        if(obj == null) return false;
        if(!(obj instanceof User)) return false;

        return new EqualsBuilder()
                .append(getId(), ((User) obj).getId())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return getId()!= null ? getId().hashCode() : 0;
    }
}
