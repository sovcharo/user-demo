package com.user.demo.user.internal;

import com.user.demo.exceptions.SequenceLoadException;
import com.user.demo.user.User;
import com.user.demo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
class UserMongoService implements UserService {

    private static final String USER = "user";

    private UserSequenceRepository sequenceRepo;

    private UserRepository userRepo;

    @Autowired
    public UserMongoService(UserRepository userRepo, UserSequenceRepository sequenceRepo) {
        this.userRepo = userRepo;
        this.sequenceRepo = sequenceRepo;
    }

    @Override
    public Collection<User> findAllUsers() {
        return userRepo
                .findAll()
                .stream()
                .map(this::generateDto)
                .collect(Collectors.toList());
    }

    @Override
    public User findUser(@NotNull Long id) {
       return generateDto(
               userRepo.findById(id)
                       .orElseThrow(
                               () -> new NotFoundException(String.format("No user found with id %d", id))));

    }

    @Override
    public Long createUser(@Valid User user) {

        validateUser(user);

        try {
            long id = sequenceRepo.getNextId(USER);
            user.setId(id);
            return userRepo.save(toEntity(user)) != null ? id : null;

        }catch (SequenceLoadException e) {
            return null;
        }
    }

    private void validateUser(@Valid User user) {
        if (user == null
                || user.getFirstName() == null
                || user.getLastName() == null
                || user.getEmail() == null
                || user.getDateOfBirth() == null) {

            throw new BadRequestException("Invalid user.");
        }
    }

    @Override
    public void updateUser(User user) {

        validateUser(user);
        userRepo.save(toEntity(user));
    }

    @Override
    public void deleteUser(@NotNull Long id) {
        userRepo.deleteById(id);
    }

    private User generateDto(UserEntity user) {
        return User
                .builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .dateOfBirth(user.getDateOfBirth())
                .build();
    }

    private UserEntity toEntity(User user) {
        return UserEntity
                .builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .dateOfBirth(user.getDateOfBirth())
                .build();
    }
}
