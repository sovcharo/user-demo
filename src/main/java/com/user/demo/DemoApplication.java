package com.user.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.ws.rs.core.Application;

@SpringBootApplication(scanBasePackages={"com.user.demo.config, com.user.demo.user"})
public class DemoApplication extends Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
