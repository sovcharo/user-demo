package com.user.demo.exceptions;

import lombok.Getter;

@Getter
public class SequenceLoadException extends Exception {

    private final String message;

    public SequenceLoadException (String msg) {
        super(msg);
        message = msg;
    }
}
