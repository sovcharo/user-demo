package com.user.demo.user.internal;


import com.user.demo.config.MongoConfig;
import com.user.demo.exceptions.SequenceLoadException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(
        classes = { MongoConfig.class, UserSequenceMongoRepository.class  },
        loader = AnnotationConfigContextLoader.class)
@DataMongoTest
public class UserSequenceMongoRepositoryTest {

    @Autowired
    private UserSequenceMongoRepository sequenceRepo;

    @Test
    public void getNextIdTest() throws SequenceLoadException {
        Long id = sequenceRepo.getNextId("user");
        assertNotNull(id);
    }
}
