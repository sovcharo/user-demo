package com.user.demo.user.internal;

import com.user.demo.config.MongoConfig;
import com.user.demo.user.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.time.LocalDate;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(
        classes = { MongoConfig.class, UserMongoService.class, UserSequenceMongoRepository.class  },
        loader = AnnotationConfigContextLoader.class)
@DataMongoTest
public class UserMongoServiceTest {

    private static final String TEST_USER_FNAME = "Horatio";
    private static final String TEST_USER_LNAME = "Nullbuilt";
    private static final String TEST_USER_EMAIL = "horation@hotbabe.com";
    private static final String NEW_TEST_FIRST_NAME = "new first name";

    private long testId;

    @Autowired
    private UserMongoService userMongoService;

    @Test
    public void startUpTest() {}

    @Before
    public void setUp() {

        User user = new User();
        user.setFirstName(TEST_USER_FNAME);
        user.setLastName(TEST_USER_LNAME);
        user.setEmail(TEST_USER_EMAIL);
        user.setDateOfBirth(LocalDate.now());
        testId =  userMongoService.createUser(user);
    }

    @After
    public void cleanChanges() {

       if(userMongoService.findUser(testId) == null) {
           setUp();
       } else {
           User user = new User();
           user.setId(testId);
           user.setFirstName(TEST_USER_FNAME);
           user.setLastName(TEST_USER_LNAME);
           user.setEmail(TEST_USER_EMAIL);
           user.setDateOfBirth(LocalDate.now());
           userMongoService.updateUser(user);
       }
    }

    @Test
    public void findAllUsersTest(){

        // tested method call
        Collection<User> users = userMongoService.findAllUsers();

        // asserts
        assertNotNull(users);
        assertFalse(users.isEmpty());
        User user = users.stream().findAny().get();
        assertEquals(TEST_USER_LNAME, user.getLastName());
        assertEquals(TEST_USER_EMAIL, user.getEmail());
    }

    @Test
    public void createUserTest(){

        // tested method call
        long id = userMongoService.createUser(
                User
                        .builder()
                        .firstName("jerry")
                        .lastName("hmmm")
                        .email("some@mail.com")
                        .dateOfBirth(LocalDate.now())
                        .build());

        // asserts
        Collection<User> allUsers = userMongoService.findAllUsers();
        allUsers.forEach(user -> {
            assertNotNull(user);
            assertNotNull(user.getId());
            assertNotNull(user.getFirstName());
            assertNotNull(user.getLastName());
            assertNotNull(user.getEmail());
            assertNotNull(user.getDateOfBirth());
        });
        assertNotNull(id);
    }

    @Test(expected = BadRequestException.class)
    public void createInvalidUserTest() {

        userMongoService.createUser(User.builder().build());
    }

    @Test(expected = NotFoundException.class)
    public void findMissingUserTest(){
        userMongoService.findUser(Long.MAX_VALUE);
    }

    @Test
    public void findUserTest() {
        // set up
        Collection<User> users = userMongoService.findAllUsers();
        Long id = users
                .stream()
                .findAny()
                .get()
                .getId();
        // call test method
        User user = userMongoService.findUser(id);

        //asserts
        assertNotNull(user.getFirstName());
        assertFalse(user.getFirstName().isEmpty());
        assertNotNull(user.getLastName());
        assertFalse(user.getLastName().isEmpty());
        assertNotNull(user.getEmail());
        assertFalse(user.getEmail().isEmpty());
        assertNotNull(user.getDateOfBirth());
    }

    @Test(expected = BadRequestException.class)
    public void updateInvalidUserTest(){
        Collection<User> users = userMongoService.findAllUsers();
        Long id = users
                .stream()
                .findAny()
                .get()
                .getId();
        // call test method
        userMongoService.updateUser(User.builder().id(id).build());
    }

    @Test
    public void updateUserTest(){

        // set up
        Collection<User> users = userMongoService.findAllUsers();
        User testUser = users
                .stream()
                .findAny()
                .get();

        testUser.setFirstName(NEW_TEST_FIRST_NAME);
        // call test method
        userMongoService.updateUser(testUser);

        // asserts
        User updatedUser = userMongoService.findUser(testUser.getId());
        assertEquals(NEW_TEST_FIRST_NAME, updatedUser.getFirstName());
    }

    @Test
    public void deleteUserTest(){
        // setup
        Collection<User> users = userMongoService.findAllUsers();
        User testUser = users
                .stream()
                .findAny()
                .get();
        // call test method
        userMongoService.deleteUser(testUser.getId());
        // assert
        Collection<User> afterDelete = userMongoService.findAllUsers();
        assertFalse(afterDelete.contains(testUser));


    }
}
